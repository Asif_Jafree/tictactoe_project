let container = document.querySelector('.container');

let first = document.createElement('div');
first.className = 'first';
let div1 = document.createElement('div');
div1.className = 'div';
div1.id = 'A0';
let div2 = document.createElement('div');
div2.className = 'div';
div2.id = 'B1';
let div3 = document.createElement('div');
div3.className = 'div';
div3.id = 'C2';

first.appendChild(div1);
first.appendChild(div2);
first.appendChild(div3);
container.appendChild(first);

let second = document.createElement('div');
second.className = 'first';
let div4 = document.createElement('div');
div4.className = 'div';
div4.id = 'D3';
let div5 = document.createElement('div');
div5.className = 'div';
div5.id = 'E4';
let div6 = document.createElement('div');
div6.className = 'div';
div6.id = 'F5';

second.appendChild(div4);
second.appendChild(div5);
second.appendChild(div6);
container.appendChild(second);

let third = document.createElement('div');
third.className = 'first';
let div7 = document.createElement('div');
div7.className = 'div';
div7.id = 'G6';
let div8 = document.createElement('div');
div8.className = 'div';
div8.id = 'H7';
let div9 = document.createElement('div');
div9.className = 'div';
div9.id = 'I8';

third.appendChild(div7);
third.appendChild(div8);
third.appendChild(div9);
container.appendChild(third);

let textDiv = document.createElement('div');
textDiv.className = 'text-div';
textDiv.innerText = 'X Play First';
container.appendChild(textDiv);

let button = document.createElement('button');
button.className = 'btn';
button.innerText = 'Restart';
container.appendChild(button);

//Create an array of Size 9
let arr = new Array(9);
arr.fill(-1); // fill all array  with -1;

//Function for check the winner
function findWinner() {
  //linear check for winner
  if (arr[0] === arr[1] && arr[1] === arr[2] && arr[1] != -1) return arr[0];
  if (arr[3] === arr[4] && arr[4] === arr[5] && arr[5] != -1) return arr[3];
  if (arr[6] === arr[7] && arr[7] === arr[8] && arr[8] != -1) return arr[6];
  // vertical check for winner
  if (arr[0] === arr[3] && arr[3] === arr[6] && arr[6] != -1) return arr[0];
  if (arr[1] === arr[4] && arr[4] === arr[7] && arr[7] != -1) return arr[1];
  if (arr[2] === arr[5] && arr[5] === arr[8] && arr[8] != -1) return arr[2];

  //Diagonal check for winner
  if (arr[0] === arr[4] && arr[4] === arr[8] && arr[0] != -1) return arr[0];
  if (arr[2] === arr[4] && arr[4] === arr[6] && arr[6] != -1) return arr[6];

  return -1;
}

// variable count even of X and odd for O turn
let count = 2;

function playGame() {
  container.addEventListener('click', (event) => {
    let target = event.target;
    //refresh Page
    if (target.className === 'btn') {
      window.location.reload();
    }
    //Check turns
    if (target.innerText === '' && count) {
      if (count % 2 === 0) {
        target.innerText = 'X';
        arr[target.id.slice(1)] = 'X';
        textDiv.innerText = 'O Turn to play';
        count++;
        console.log(arr);
      } else {
        target.innerText = 'O';
        arr[target.id.slice(1)] = 'O';
        textDiv.innerText = 'X Turn to play';
        count++;
      }
      const winner = findWinner(arr);
      //Check there is any Winner Till now
      if (winner != -1) {
        textDiv.innerText = `Congratulation "${winner}" win the Game`;
        textDiv.className = 'winner';
        count = 0;
      }
      //Check draw condition
      if (count === 11 && winner === -1) {
        
        textDiv.innerText = `Game Draw`;
      }
    }
  });
}

playGame();
